INTRODUCTION
------------
This module provides a field formatter to display images with
Bootstrap 3 Lightbox plugin (http://ashleydw.github.io/lightbox/).

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/tbosviel/2566301

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2566301

REQUIREMENTS
------------
 * Bootstrap 3.x or other theme which uses Bootstrap 3 framework
   (https://drupal.org/project/bootstrap)
 * Libraries API 2.x (https://drupal.org/project/libraries)

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Download and unpack the Bootstrap 3 Lightbox plugin into your libraries
   folder (this will usually be "sites/all/libraries"). It's available at:
   https://github.com/ashleydw/lightbox/releases/tag/v3.3.2
   Proper libraries structure:
    - libraries/
      - ekko-lightbox/
        - dist/
          - ekko-lightbox.css
          - ekko-lightbox.js
          - ekko-lightbox.min.css
          - ekko-lightbox.min.js
        - examples/
        - [...]
        - package.json
        - README.md

CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will expose the Bootstrap Lightbox formatter to Field API.
