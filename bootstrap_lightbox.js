/**
 * @file
 * Bootstrap Lightbox library integration.
 */

(function($) {
  Drupal.behaviors.ekkoLightbox = {
    attach: function (context, settings) {
      $(document, context).delegate('*[data-toggle="lightbox"]', 'click', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
      });
    }
  };
})(jQuery);
