<?php
/**
 * @file
 * Bootstrap Lightbox theme functions.
 */

/**
 * Returns HTML for a Bootstrap Lightbox image field formatter.
 *
 * @param $variables
 *   An associative array containing:
 *   - item: Associative array of image data, which may include "uri", "alt",
 *     "width", "height", "title" and "attributes".
 *   - display_settings: Associative array of field display settings.
 *
 * @ingroup themeable
 */
function theme_bootstrap_lightbox_formatter($variables) {
  $item = $variables['item'];
  $settings = $variables['display_settings'];

  $image = array(
    'path' => $item['uri'],
  );

  if (array_key_exists('alt', $item)) {
    $image['alt'] = $item['alt'];
  }

  if (isset($item['attributes'])) {
    $image['attributes'] = $item['attributes'];
  }

  if (isset($item['width']) && isset($item['height'])) {
    $image['width'] = $item['width'];
    $image['height'] = $item['height'];
  }

  // Do not output an empty 'title' attribute.
  if (isset($item['title']) && drupal_strlen($item['title']) > 0) {
    $image['title'] = $item['title'];
  }

  // Generate HTML for the content image.
  if ($settings['content_image_style']) {
    $image['style_name'] = $settings['content_image_style'];
    $output = theme('image_style', $image);
  }
  else {
    $output = theme('image', $image);
  }

  // Setting up the lightbox with data attributes.
  $options['attributes'] = array(
    'data-toggle' => 'lightbox',
    'data-gallery' => $settings['lightbox_gallery_id'],
  );

  // If the image title is empty use alt for the lightbox title.
  if (isset($image['title'])) {
    $options['attributes']['data-title'] = $image['title'];
  }
  elseif (isset($image['alt'])) {
    $options['attributes']['data-title'] = $image['alt'];
  }

  // Build the URL for the lightbox image.
  if ($settings['lightbox_image_style']) {
    $path = image_style_url($settings['lightbox_image_style'], $item['uri']);
  }
  else {
    $path = file_create_url($item['uri']);
  }

  // When displaying an image inside a link, the html option must be TRUE.
  $options['html'] = TRUE;

  return l($output, $path, $options);
}
